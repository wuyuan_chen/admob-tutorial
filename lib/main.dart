import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:ads_tutorial2/ad_manager.dart';

// You can also test with your own ad unit IDs by registering your device as a
// test device. Check the logs for your device's ID value.
// const String testDevice = '80149BAF-653B-4339-9415-458899565BCF';
const String testDevice = "emulator-5554";
void main() => runApp(TestAds());

class TestAds extends StatefulWidget {
  @override
  _TestAdsState createState() => _TestAdsState();
}

class _TestAdsState extends State<TestAds> {
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    testDevices: testDevice != null ? <String>[testDevice] : null,
    keywords: <String>['foo', 'bar'],
    nonPersonalizedAds: true,
  );

  BannerAd _bannerAd;
  InterstitialAd _interstitialAd;

  Future<String> _getId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      // import 'dart:io'
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  BannerAd createBannerAd() {
    return BannerAd(
      // adUnitId: BannerAd.testAdUnitId,
      adUnitId: AdManager.bannerAdUnitId,
      size: AdSize.leaderboard,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("BannerAd event $event");
        // if (event == MobileAdEvent.closed) {
        //   showBannerAd();
        // }
      },
    );
  }

  void showBannerAd() {
    _bannerAd = createBannerAd()
      ..load()
      ..show(
        anchorOffset: 0.0,
        // Positions the banner ad 10 pixels from the center of the screen to the right
        // horizontalCenterOffset: -100.0,
        anchorType: AnchorType.bottom,
      );
  }

  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
      // Replace the testAdUnitId with an ad unit id from the AdMob dash.
      // https://developers.google.com/admob/android/test-ads
      // https://developers.google.com/admob/ios/test-ads
      // adUnitId: InterstitialAd.testAdUnitId,
      adUnitId: AdManager.interstitialAdUnitId,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("InterstitialAd event is $event");
        if (event == MobileAdEvent.closed) {
          // _interstitialAd = createInterstitialAd()..load();
        }
      },
    );
  }

  void showInterstitialAd() {
    _interstitialAd = createInterstitialAd()
      ..load()
      ..show();
  }

  @override
  void initState() {
    _getId().then((id) {
      print("---->$id");
    });
    FirebaseAdMob.instance.initialize(
      // appId: BannerAd.testAdUnitId,
      appId: AdManager.appId,
    );
    showBannerAd();
    _interstitialAd = createInterstitialAd()..load();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    _bannerAd.dispose();
    _interstitialAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Test Ads",
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("hello world"),
              FlatButton(
                child: Text(
                  "Interstitial Ad",
                ),
                onPressed: showInterstitialAd,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
